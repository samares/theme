<?php
/**
 * @version    1.0
 * @package    WR_Theme
 * @author     WooRockets Team <support@woorockets.com>
 * @copyright  Copyright (C) 2014 WooRockets.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.woorockets.com
 */

// Get theme options
$wr_nitro_options = WR_Nitro::get_options();

// Get footer layout
$wr_layout = $wr_nitro_options['footer_layout'];

// Get sidebar
$wr_sidebar_1 = $wr_nitro_options['footer_sidebar_1'];
$wr_sidebar_2 = $wr_nitro_options['footer_sidebar_2'];
$wr_sidebar_3 = $wr_nitro_options['footer_sidebar_3'];
$wr_sidebar_4 = $wr_nitro_options['footer_sidebar_4'];
$wr_sidebar_5 = $wr_nitro_options['footer_sidebar_5'];
?>
		<?php do_action( 'wr_nitro_before_footer' ); ?>

		<footer id="footer" class="<?php
			if ( is_customize_preview() )
				echo 'customizable customize-section-footer ';
 		?>footer" <?php WR_Nitro_Helper::schema_metadata( array( 'context' => 'footer' ) ); ?>>

 			<?php if ( ! empty( $wr_nitro_options['sidebar_before_footer_widget'] ) && is_active_sidebar( $wr_nitro_options['sidebar_before_footer_widget'] ) ) : ?>
				<div class="sidebar-before-footer">
					<div class="container">
						<?php dynamic_sidebar( $wr_nitro_options['sidebar_before_footer_widget'] ); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php if ( is_active_sidebar( $wr_sidebar_1 ) || is_active_sidebar( $wr_sidebar_2 ) || is_active_sidebar( $wr_sidebar_3 ) || is_active_sidebar( $wr_sidebar_4 ) || is_active_sidebar( $wr_sidebar_5 ) ) : ?>
			<div class="top">
				<div class="top-inner">
					<div class="row">
						<?php
							switch( $wr_layout ) :
								case 'layout-1' :
									echo '<div class="cm-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									break;

								case 'layout-2' :
									echo '<div class="cm-9 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-3 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									break;

								case 'layout-3' :
									echo '<div class="cm-6 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									break;

								case 'layout-4' :
									echo '<div class="cm-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									break;

								case 'layout-5' :
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-6 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									break;

								case 'layout-6' :
									echo '<div class="cm-3 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-9 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									break;

								case 'layout-8' :
									echo '<div class="cm-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									break;

								case 'layout-9' :
									echo '<div class="cm-4 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-8 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									break;

								case 'layout-10' :
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-6 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-3 w800-4 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									break;
								case 'layout-11' :
									echo '<div class="cm-4 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_4 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_5 );
									echo '</div>';
									break;
								case 'layout-12' :
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									echo '<div class="cm-2 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_4 );
									echo '</div>';
									echo '<div class="cm-4 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_5 );
									echo '</div>';
									break;
								default :
									echo '<div class="cm-3 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_1 );
									echo '</div>';
									echo '<div class="cm-3 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_2 );
									echo '</div>';
									echo '<div class="cm-3 w800-6 w800-clear cxs-12">';
										dynamic_sidebar( $wr_sidebar_3 );
									echo '</div>';
									echo '<div class="cm-3 w800-6 cxs-12">';
										dynamic_sidebar( $wr_sidebar_4 );
									echo '</div>';
									break;
							endswitch;
						?>
					</div><!-- .row -->
				</div><!-- .top-inner -->
			</div>
			<?php endif; ?>

			<?php if ( ! empty( $wr_nitro_options['sidebar_after_footer_widget'] ) && is_active_sidebar( $wr_nitro_options['sidebar_after_footer_widget'] ) ) : ?>
				<div class="sidebar-after-footer">
					<div class="container">
						<?php dynamic_sidebar( $wr_nitro_options['sidebar_after_footer_widget'] ); ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="bot">
				<div class="info">
					<?php echo do_shortcode( wp_kses_post( $wr_nitro_options['footer_bot_text'] ) ); ?>
				</div>
			</div>
			<?php // Back to top button
				if ( $wr_nitro_options['back_top'] ) :
					$btn_classes = array( 'heading-color hover-bg-primary dib' );
					if ( 'light' == $wr_nitro_options['back_top_style'] ) {
						$btn_classes[] = 'overlay_bg nitro-line';
					} else {
						$btn_classes[] = 'heading-bg';
					}
				?>
					<div id="wr-back-top" >
						<a href="javascript:void(0);" class="<?php echo esc_attr( implode(' ', $btn_classes ) ); ?>"  title="<?php esc_attr_e( 'Back to top', 'wr-nitro' ); ?>"><i class="fa fa-angle-up"></i></a>
					</div>
			<?php endif; ?>
		</footer><!-- .footer -->

		<?php do_action( 'wr_nitro_after_footer' ); ?>
	</div></div><!-- .wrapper -->
	
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   


<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<!-- jQuery --> 

<!-- jQuery easing plugin --> 
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easing.min.js" type="text/javascript"></script> 
<script>
$(function() {

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})

});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
function history()
   {

						$(".boxesrow_main").toggle("fast");
	  
   }
</script>
	
<?php wp_footer(); ?>

</body>
</html>
