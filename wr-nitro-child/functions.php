<?php
/**
 * @version    1.0
 * @package    Nitro
 * @author     WooRockets Team <support@woorockets.com>
 * @copyright  Copyright (C) 2014 WooRockets.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.woorockets.com
 */

/**
 * Enqueue script for child theme
 */
function wr_nitro_child_enqueue_scripts() {
	wp_enqueue_script( 'nitro-child-custom-script', get_stylesheet_directory_uri() . '/js/main.js', array(), '1.0.0', true);	
	
	wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_enqueue_style( 'nitro-child-custom-style', get_stylesheet_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'wr-nitro-child-style', get_stylesheet_directory_uri() . '/style.css' );

    wp_register_script( 'jquery-easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', array(), '1.0.0', true );
    wp_register_script( 'main-form-js', get_stylesheet_directory_uri() . '/js/main-form.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'flatpickr-js', get_stylesheet_directory_uri() . '/js/flatpickr.min.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'phone-mask-js', get_stylesheet_directory_uri() . '/js/mask.min.js', array('jquery', 'main-form-js'), '1.0.0', true );

    wp_register_style( 'flatpickr-css', get_stylesheet_directory_uri() . '/css/flatpickr.min.css', array(), '1.0.0' );
    wp_register_style( 'main-form-css', get_stylesheet_directory_uri() . '/css/main-form.css', array(), '1.0.0' );

}
add_action( 'wp_enqueue_scripts', 'wr_nitro_child_enqueue_scripts', 1000000000 );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/*carbon-fiedls include*/
require_once get_stylesheet_directory() .'/inc/carbon-fields/carbon-fields-plugin.php';
/*Theme options*/
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'inc/custom-fields/theme-options.php' );
	require_once( 'inc/custom-fields/page-options.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}
/*end carbon-fields*/

function post_title_shortcode(){
    return get_the_title();
}
add_shortcode('post_title','post_title_shortcode');

add_shortcode('product_data','custom_product_function');
function custom_product_function($atts)
{
    $post_id = $atts['id'];
    $title = get_the_title($post_id);
    $link = get_the_permalink($post_id);
    $image = get_the_post_thumbnail($post_id);
    // $short_desc = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
    // $short_desc = $product->get_short_description();
    $short_desc = get_the_excerpt($post_id);
    	
	$data = '
	<div class="col-lg-6 offset-lg-1">
		<div class="row">
			<div class="col-12">
				<div class="camp-curr-goal">Current Goal: '.$title.'</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="curr-gaol-img-wrap">
				'.$image.'
				</div>
			</div>
			<div class="col-md-7">
				<div class="row">
					<div class="col-12">
					<div class="curr-goal-desc-title">Gift Set Description</div>
					<div class="curr-goal-desc-val">
						'.$short_desc.'
						
					</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row linked_product_feature">
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 1</div>
			</div>
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 2</div>
			</div>
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 3</div>
			</div>
		</div>
		</div>
		<script>		
			jQuery(document).ready(function($){		
			// Code goes here
				$(".custom-campaign-data-val").html("'.$title.'");
			});
		</script>
	';

    return $data;
}


//Set form
function set_form_process() {

    $meta = array();

    $meta['_recipients_gift'] = (!$_POST['gift-recipient']) ? 'For Myself' : 'For someone else';
    $title = ($_POST['rcpts_name']) ? sanitize_text_field($_POST['rcpts_name']) : 'Not set';
    $meta['_recipients_name'] = $title;
    $meta['_recipient_age'] = sanitize_text_field($_POST['rcpnt-age']);
    $meta['_recipient_gender'] = sanitize_text_field($_POST['recipient-gender']);
    $meta['_recipients_relationship'] = sanitize_text_field($_POST['recipient-relationship']);
    $meta['_recipients_occasion'] = sanitize_text_field($_POST['gift-occasion']);
    //price and product set
    $d = explode('|',$_POST['set-goal']);
    //price
    $meta['_nf_funding_goal'] = $d[1];
    //choosen set
    $meta['_recipients_set_goal'] = get_the_title($d[0]) . ' (Product ID: ' .$d[0]. ')';
    $meta['_nf_duration_start'] = date('d.m.Y H:i');
    $meta['_nf_duration_end'] = $_POST['pref-date'] .' '. $_POST['pref-time'];
    $meta['_picked_date'] = sanitize_text_field($_POST['pref-date']);
    $meta['_recipients_picked_time'] = sanitize_text_field($_POST['pref-time']);
    $meta['_recipients_delivery'] = (!$_POST['gift-delivery']) ? 'To me' : 'To recipient';
    $meta['_recipients_address1'] = sanitize_text_field($_POST['addr1']);
    $meta['_recipients_address2'] = sanitize_text_field($_POST['addr2']);
    $meta['_recipients_city'] = sanitize_text_field($_POST['addr-city']);
    $meta['_recipients_state'] = sanitize_text_field($_POST['addr-state']);
    $meta['_recipients_zip'] = sanitize_text_field($_POST['addr-zip']);
    $meta['_recipients_phone'] = sanitize_text_field($_POST['phone']);

    $curUserId = (get_current_user_id()) ? get_current_user_id() : 1;

    $postDate= array(
        'post_title'    => $title,
        'post_status'   => 'publish',
        'post_type'   => 'product',
        'post_author'   => $curUserId
    );
    
    $id = wp_insert_post( $postDate, true);

    if( !is_wp_error($id) ){

        //set product type
        wp_set_object_terms( $id, 'crowdfunding', 'product_type' );

        //up date post meta if count > 0
        if(count($meta) > 0){

            foreach ($meta as $key => $val) {

                update_post_meta( $id, $key, $val );

            }
        }
    }

    wp_redirect( $_SERVER["HTTP_REFERER"] . '?mess=true' );
    exit;

}
add_action( 'admin_post_nopriv_set_form', 'set_form_process' );
add_action( 'admin_post_set_form', 'set_form_process' );
//Set form END