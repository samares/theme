<?php 
use Carbon_Fields\Container;
use Carbon_Fields\Field;


Container::make( 'post_meta', 'Campaign Data' )
	 ->show_on_post_type('product')
		 ->add_fields(array(
			  Field::make( 'text', 'recipients_gift', 'For whom you plan this box?' ),
			  Field::make( 'text', 'recipients_name', 'Recipients name:' ),
			  Field::make( 'text', 'recipient_age', ' Recipient Age: ' ),
			  Field::make( 'text', 'recipient_gender', 'Recipient Gender:' ),
			  Field::make( 'text', 'recipients_relationship', 'Relationship with recipient:' ),
			  Field::make( 'text', 'recipients_occasion', 'Occasion:' ),
			  Field::make( 'text', 'recipients_set_goal', 'Choosen Gift Set:' ),
			  Field::make( 'text', 'picked_date', 'Picked Date:' ),
			  Field::make( 'text', 'recipients_picked_time', 'Picked Time:' ),
			  Field::make( 'text', 'recipients_delivery', 'Choosen delivery place:' ),
			  Field::make( 'text', 'recipients_address1', 'Address line 1:' ),
			  Field::make( 'text', 'recipients_address2', 'Address line 2:' ),
			  Field::make( 'text', 'recipients_city', 'City:' ),
			  Field::make( 'text', 'recipients_state', 'State:' ),
			  Field::make( 'text', 'recipients_zip', 'Zip:' ),
			  Field::make( 'text', 'recipients_phone', 'Phone:' ),
		 ));