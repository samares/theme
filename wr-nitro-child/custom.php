<?
function load_style_script(){
	wp_enqueue_script('parallax', 'https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js','', null, true );
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/main.css');
}
add_action('wp_enqueue_scripts', 'load_style_script');